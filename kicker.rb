#!/usr/bin/ruby


require 'bunny'
require 'msgpack'
require 'thread'


$stdout.sync = true

def count_loop
	
	last_p = 0
	last_c = 0
	
	loop do
		$mutex.lock
		$cond.wait( $mutex, 0.5 )
		t_p, t_c = $total_prod, $total_cons
		$mutex.unlock
		next if last_p == t_p and last_c == t_c
		last_p = t_p
		last_c = t_c
		puts " -  Prod / Cons: #{t_p} / #{t_c}"
		next if t_p == 0
		break if t_p == t_c
	end
	
	duration = Time.now - $start_time
	puts "[*] Test completed"
	puts " -  Number of messages: #{$total_prod}"
	puts " -  Duration: #{duration}"
	puts " -  Average speed: #{$total_prod / duration}"
	
	begin
		$ch.close
		$conn.close
	rescue
		# ignore
	end
	
	$cons.keys.each { |pid| Process.kill( 'KILL', pid ) rescue true }
	
	exit 0
	
end


if ARGV.size < 3
	$stderr.puts "Usage: #{$0} <no_msgs> <no_prods> <no_cons>"
	exit 1
end

$msgs = ARGV.shift.to_i
$no_prods = ARGV.shift.to_i
$no_cons = ARGV.shift.to_i
$cons = {}
$prod = {}
$total_prod = 0
$total_cons = 0

$mutex = Mutex.new
$cond = ConditionVariable.new

$dir = File.dirname( __FILE__ )
$log_dir = File.join( $dir, 'exec-logs' )

# start the consumers
$no_cons.times do
	pid = Process.spawn( File.join( $dir, 'consumer.rb' ) )
	$cons[pid] = 0
end

# start the producers
$no_prods.times do |iter|
	pid = Process.spawn( "#{File.join( $dir, 'producer.rb' )} #{$msgs.to_s} 2>&1 | tee #{File.join( $log_dir, "prod-#{iter}" )}" )
	$prod[pid] = 0
	Process.detach pid
end

$conn = Bunny.new
$conn.start

$ch = $conn.create_channel nil, 16
q_prod = $ch.queue( "rabbitmq.test.count.prod", auto_delete: true )
q_cons = $ch.queue( "rabbitmq.test.count.cons", auto_delete: true )
ex_kick = $ch.fanout 'kick'

q_prod.subscribe do |delivery_info, metadata, body|
	msg = MessagePack.load body
	$mutex.lock
	$prod[msg['pid']] = msg['count']
	$total_prod = $prod.values.reduce :+
	$cond.signal
	$mutex.unlock
end

q_cons.subscribe do |delivery_info, metadata, body|
	msg = MessagePack.load body
	$mutex.lock
	$cons[msg['pid']] = msg['count']
	$total_cons = $cons.values.reduce :+
	$cond.signal
	$mutex.unlock
end

sleep 2
Thread.abort_on_exception = true
Thread.new { count_loop() }

puts "[*] Starting the test ..."
$start_time = Time.now
ex_kick.publish "GO"

loop do
	sleep 60
end



#!/usr/bin/ruby

Thread.abort_on_exception = true

require 'bunny'
require 'msgpack'


INTERVAL = 0.5


def count_loop
	
	last_cnt = 0
	cnt = 0
	loop do
		sleep INTERVAL
		next if $count == 0
		cnt = $count
		next if cnt == last_cnt
		$count_ex.publish( MessagePack.dump( {pid: $$.to_i, count: cnt} ), routing_key: $count_q.name )
		last_cnt = cnt
	end
	
end


conn = Bunny.new
conn.start

ch = conn.create_channel
test_q = ch.queue( "rabbitmq.test.queue", auto_delete: true )
$count_q = ch.queue( "rabbitmq.test.count.cons", auto_delete: true )
$count_ex = ch.default_exchange

$count = 0

Thread.new { count_loop() }

test_q.subscribe do |delivery_info, metadata, body|
	msg = MessagePack.load body
	$count += 1
end

loop do
	sleep 60
end


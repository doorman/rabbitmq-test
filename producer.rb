#!/usr/bin/ruby

Thread.abort_on_exception = true

require 'bunny'
require 'msgpack'


if ARGV.empty?
	$stderr.puts "Usage: #{$0} <no_msgs>"
	exit 1
end

$done = false
$started = false

no_msgs = ARGV.shift.to_i
msg = { server: "abcdef", ip: "123.456.789.101", nicknames: ["srv", "host", "blabla"] }

conn = Bunny.new
conn.start

ch = conn.create_channel
start_ex = ch.fanout 'kick'
start_q = ch.queue '', exclusive: true
test_q = ch.queue "rabbitmq.test.queue", auto_delete: true
count_q = ch.queue "rabbitmq.test.count.prod", auto_delete: true
test_ex = ch.default_exchange

start_q.bind start_ex

start_q.subscribe do |delivery_info, properties, body|
	unless $started
		$started = true
		negatives = 0
		no_msgs.times do
			begin
				test_ex.publish MessagePack.dump( msg ), routing_key: test_q.name
			rescue => err
				negatives += 1
				$stdout.puts "Err: #{err.inspect}"
			end
		end
		begin
			test_ex.publish MessagePack.dump( {pid: $$.to_i, count: no_msgs - negatives} ), routing_key: count_q.name
		rescue => err
			$stdout.puts "Could not send notif because of: #{err.inspect}"
		end
		$done = true
	end
end

loop do
	sleep 2
	break if $done
end

begin
	ch.close
	conn.close
rescue
	# ignore
end

exit 0

